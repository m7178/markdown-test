#ifndef _MAINLOOP_H_
#define _MAINLOOP_H_

/** \mainpage
 * Main state machine
 * \dot
 * digraph statemachine {
 *      rankdir=UD;	 
 *      	 
 *      node [shape=record];
 *		stInit [ label="Init", color = orange];
 *      stOff [ label="Off" , color = red];
 *      stAuto [ label="Auto", color = green ];
 *
 *		stInit -> stOff [ label ="(1)", arrowhead="open", style="solid" , color = red];
 *
 *		stOff -> stOff [ label ="(2)" , arrowhead="open", style="solid" , color = red];
 *		stOff -> stAuto [ label ="(3)", arrowhead="open", style="solid" , color = green];
 *
 *		
 *  }
 *  \enddot
*/

#endif